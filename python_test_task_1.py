lst = [0, 1, 245.0, 3.17, "2.50", "eight", False, "", True]
print('Value is not a numeric type =', [x for x in lst if not type(x) == int and not type(x) == float])
print('Value is an integer =', [x for x in lst if type(x) == int])
print('Value is an float =',[x for x in lst if type(x) == float])